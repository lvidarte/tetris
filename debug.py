import sys
import json

with open('debug.log') as f:
    doc = json.loads(f.readlines()[-1])
    print("Board")
    for row in doc['board']:
        for col in row:
            if col:
                print('# ', end='')
            else:
                print('. ', end='')
        print()
    print("Next")
    for row in doc['next']['pieces'][doc['next']['actual']]:
        for col in row:
            if col:
                print('# ', end='')
            else:
                print('. ', end='')
        print()
    print(f"Coords {doc['next']['coords']}")
