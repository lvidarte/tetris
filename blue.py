"""
Author: Leo Vidarte <http://nerdlabs.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

BG_COLOR = '#333'

# Board
BOARD_BG_COLOR = 'blue'
BOARD_FG_COLOR = 'white'
BOARD_GRID_COLOR = 'black'

# Status
FONT_SIZE = 12
FONT_COLOR = 'white'

# Tetrominos
TETROMINO_FG_COLOR = 'black'
TETROMINO_BORDER_WIDTH = 4 # in pixels
I_COLOR = 'white'
O_COLOR = 'white'
T_COLOR = 'white'
J_COLOR = 'white'
L_COLOR = 'white'
S_COLOR = 'white'
Z_COLOR = 'white'
COMPLETE_ROW_BG_COLOR = 'black' # None for inherit
COMPLETE_ROW_FG_COLOR = 'white'
